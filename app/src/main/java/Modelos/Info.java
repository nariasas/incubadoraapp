package Modelos;

/**
 * Created by Nicolas on 23/04/2018.
 */

public class Info {

    public String Nombre;
    public String Valor;
    public boolean Admin;
    public int Image;

    public Info() {

    }

    public Info(String _Nombre, String _Valor, boolean _Admin) {
        Nombre = _Nombre;
        Valor = _Valor;
        Admin = _Admin;
    }

    public Info(String _Nombre, String _Valor, boolean _Admin, int _image) {
        Nombre = _Nombre;
        Valor = _Valor;
        Admin = _Admin;
        Image = _image;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }

    public boolean getAdmin() {
        return Admin;
    }

    public void setAdmin(boolean Admin) {
        Admin = Admin;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }
}
