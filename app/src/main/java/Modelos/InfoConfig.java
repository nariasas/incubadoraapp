package Modelos;

import Sistema.NDebug;

/**
 * Created by nicolas on 25/04/18.
 */

public class InfoConfig {

    public String TempRef;
    public String HumRef;
    public String TempFin;
    public String HumFin;
    public String Duracion;
    public String Volteos;
    public String Minutos;
    public String Nombre;

    @Override
    public String toString() {
        String dag =
                String.valueOf(Double.parseDouble(TempRef) * 10).replace(".0", "") + ";"
                        + String.valueOf(Double.parseDouble(HumRef) * 10).replace(".0", "") + ";"
                        + String.valueOf(Double.parseDouble(TempFin) * 10).replace(".0", "") + ";"
                        + String.valueOf(Double.parseDouble(HumFin) * 10).replace(".0", "") + ";"
                        + Duracion + ";"
                        + Volteos + ";"
                        + Minutos + " "
                        + Nombre;
        NDebug.Log(dag + " Guardado!");
        return dag;
    }

    public void SetObject(String data) {

        String[] item = data.split("|");
        String[] items = item[1].split(";");
        TempRef = items[0];
        HumRef = items[1];
        TempFin = items[2];
        HumFin = items[3];
        Duracion = items[4];
        Volteos = items[5];
        Minutos = items[6];
        Nombre = item[0];
    }


}
