package Sistema;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by nicolas on 21/04/18.
 */

public class Alerta {

    public static void ErrorAlert(Context context, String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Se presentó un problema");
        dialog.setMessage(msg);
        dialog.show();
    }

    public static void SuccessAlert(Context context, String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Proceso completo");
        dialog.setMessage(msg);
        dialog.show();
    }

    public static ProgressDialog LoadingDialogOpen(Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("Realizando conexión...");
        dialog.show();
        return dialog;
    }

    public static void LoadingDialogClose(ProgressDialog dialog) {
        if (dialog != null)
            dialog.dismiss();
    }

}
