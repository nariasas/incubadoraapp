package Sistema;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by nicolas on 25/04/18.
 */

public class Preferencias {


    public static void SetPreferencia(Context context, String key, String value) {

        SharedPreferences preferences = context.getSharedPreferences("Preferencias_incuba", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String GetPreferencia(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences("Preferencias_incuba", Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

}
