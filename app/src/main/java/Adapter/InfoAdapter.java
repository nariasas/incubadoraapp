package Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import Modelos.Info;
import incubadora.nasoft.com.incubadoraapp.R;

/**
 * Created by Nicolas on 23/04/2018.
 */

public class InfoAdapter extends ArrayAdapter<Info> {

    private boolean isAdmin;

    public InfoAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public InfoAdapter(Context context, int resource, List<Info> items) {
        super(context, resource, items);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Info p = getItem(position);

        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            viewHolder = new ViewHolder();
            if (p.getNombre().equals("Días Restantes")) {
                v = vi.inflate(R.layout.info_list_progress, null);
                viewHolder.progressBar = v.findViewById(R.id.info_list_progress);
            } else {
                v = vi.inflate(R.layout.info_list, null);
            }
            viewHolder.txtNombre = v.findViewById(R.id.info_list_name);
            viewHolder.txtValor = v.findViewById(R.id.info_list_value);
            viewHolder.Imagen = v.findViewById(R.id.info_list_img);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        if (p.getImage() > 0) {
            viewHolder.Imagen.setImageDrawable(v.getContext().getDrawable(p.getImage()));
        } else {
            viewHolder.Imagen.setImageDrawable(v.getContext().getDrawable(R.drawable.ic_configurar));
        }
        if (p.getNombre().equals("Días Restantes")) {
            int val = Integer.valueOf(p.getValor());
            viewHolder.progressBar.setProgress(((18 - val) * 100) / 18);
            viewHolder.txtValor.setText(p.getValor() + " de 18");
        } else {
            viewHolder.txtValor.setText(p.getValor());
        }
        viewHolder.txtNombre.setText(p.getNombre());


        return v;
    }

    static class ViewHolder {
        TextView txtNombre;
        TextView txtValor;
        ImageView Imagen;
        ProgressBar progressBar;
    }
}
