package incubadora.nasoft.com.incubadoraapp;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Sistema.Bluetooth;
import Sistema.NDebug;

/**
 * Created by nicolas on 21/04/18.
 */

public class MainController {

    private MainActivity vista;
    private Bluetooth blue;

    public MainController(MainActivity _vista) {
        vista = _vista;
        blue = new Bluetooth(vista);
    }

    public void ObtenerListadoDispositivosBT() {
        blue.enableBluetooth();
        List<BluetoothDevice> dispositivos = blue.getPairedDevices();
        ListView liDispositivos = vista.getLiDispositivos();
        liDispositivos.setAdapter(null);
        ArrayAdapter<String> sDispositivos = new ArrayAdapter<>(vista, android.R.layout.simple_list_item_1, new ArrayList<String>());
        for (BluetoothDevice dispositivo : dispositivos) {
            sDispositivos.add(dispositivo.getName());
        }
        if (sDispositivos.getCount() <= 0) {
            sDispositivos.add("No se encontraron dispositivos vinculados");
        }
        liDispositivos.setAdapter(sDispositivos);
        sDispositivos.notifyDataSetChanged();
    }

    public AdapterView.OnItemClickListener SeleccionarDispositivo = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            String nombreDispositivo = ((TextView) view).getText().toString();
            if (!nombreDispositivo.equals("No se encontraron dispositivos vinculados")) {
                NDebug.Log(nombreDispositivo);
                Intent abrirHome = new Intent(vista, HomeActivity.class);
                abrirHome.putExtra("Dispositivo", nombreDispositivo);
                vista.startActivity(abrirHome);
            }
        }
    };

    public void AbrirVentanaDeConfiguracion() {
        Intent configPage = new Intent(vista, SendConfigActivity.class);
        vista.startActivity(configPage);
    }
}
