package incubadora.nasoft.com.incubadoraapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ConfigActivity extends AppCompatActivity {


    public EditText txtTempRef;
    public EditText txtHumRef;
    public EditText txtTempEco;
    public EditText txtHumEco;
    public EditText txtDiasIncu;
    public EditText txtVolterosDia;
    public EditText txtMinutosRecircula;
    public EditText txtNombreConfiguracion;

    Button btnGuardar;
    ConfigController controlador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Configuración");
        controlador = new ConfigController(this);
        ConfigurarActividad();
    }

    private void ConfigurarActividad() {
        txtTempRef = findViewById(R.id.txtTempRef);
        txtHumRef = findViewById(R.id.txtHumRef);
        txtTempEco = findViewById(R.id.txtTempEclo);
        txtHumEco = findViewById(R.id.txtHumEclo);
        txtDiasIncu = findViewById(R.id.txtDiasIncu);
        txtVolterosDia = findViewById(R.id.txtVolteosDia);
        txtMinutosRecircula = findViewById(R.id.txtMinutosReci);
        txtNombreConfiguracion = findViewById(R.id.txtNombreConfiguracion);
        btnGuardar = findViewById(R.id.btnGuardarConfig);
        btnGuardar.setOnClickListener(controlador.GuardarConfiguracion);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
