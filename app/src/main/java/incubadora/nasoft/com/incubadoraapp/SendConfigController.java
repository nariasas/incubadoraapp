package incubadora.nasoft.com.incubadoraapp;

import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import Sistema.Bluetooth;
import Sistema.NDebug;
import Sistema.Preferencias;

/**
 * Created by nicolas on 30/04/18.
 */

public class SendConfigController {

    private Bluetooth blue;
    private SendConfigActivity vista;
    private ArrayAdapter<String> arrays;
    private ArrayAdapter<String> adapterNombres;

    public SendConfigController(SendConfigActivity _vista) {
        vista = _vista;
        blue = new Bluetooth(vista);
    }

    public void LoadPreference() {
        String configuraciones = Preferencias.GetPreferencia(vista, "Programas");
        String[] data = configuraciones.split("\n");
        Spinner ddl = vista.getDdlConfiguraciones();
        arrays = new ArrayAdapter<String>(vista, android.R.layout.simple_list_item_1, data);


        ArrayList<String> nombres = new ArrayList<>();

        for (String item : data) {
            String[] registro = item.split(" ");
            if (registro.length > 1) {
                nombres.add(registro[1]);
            }
        }
        adapterNombres = new ArrayAdapter<String>(vista, android.R.layout.simple_list_item_1, nombres);
        ddl.setAdapter(adapterNombres);
        adapterNombres.notifyDataSetChanged();
    }

    public View.OnClickListener LoadConfiguraciones = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            String nombre = vista.getNombreDispositivo();
            Spinner ddl = vista.getDdlConfiguraciones();
            final int value = ddl.getSelectedItemPosition();

            String item = adapterNombres.getItem(value);
            String configuraciones = Preferencias.GetPreferencia(vista, "Programas");
            String[] data = configuraciones.split("\n");
            String TramaEnvio = "";

            for (String trama : data) {
                String[] registro = trama.split(" ");
                if (registro.length > 1) {
                    if (item.equals(registro[1])) {
                        NDebug.Log(registro[0].replace(".0", ""));
                        TramaEnvio = registro[0].replace(".0", "");
                    }
                }
            }
            final String finalTramaEnvio = TramaEnvio;
            blue.setOnConnect(new Bluetooth.OnConnect() {
                @Override
                public void onConnect(BluetoothDevice device, boolean exitoso, String msg) {
                    blue.send(finalTramaEnvio);
                    blue.disconnect();

                }
            });

            blue.connectToName(nombre);
        }
    };

}
