package incubadora.nasoft.com.incubadoraapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {
            Thread.sleep(1000);
            startActivity(new Intent(this, MainActivity.class));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
