package incubadora.nasoft.com.incubadoraapp;

import android.app.Activity;
import android.content.pm.ConfigurationInfo;
import android.view.View;

import Modelos.InfoConfig;
import Sistema.Alerta;
import Sistema.Preferencias;

/**
 * Created by nicolas on 25/04/18.
 */

public class ConfigController {


    private ConfigActivity vista;

    public ConfigController(ConfigActivity _vista) {
        vista = _vista;
    }


    public View.OnClickListener GuardarConfiguracion = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            InfoConfig infoConfig = new InfoConfig();
            infoConfig.Nombre = vista.txtNombreConfiguracion.getText().toString();
            infoConfig.TempRef = vista.txtTempRef.getText().toString();
            infoConfig.HumRef = vista.txtHumRef.getText().toString();
            infoConfig.TempFin = vista.txtTempEco.getText().toString();
            infoConfig.HumFin = vista.txtHumEco.getText().toString();
            infoConfig.Duracion = vista.txtDiasIncu.getText().toString();
            infoConfig.Volteos = vista.txtVolterosDia.getText().toString();
            infoConfig.Minutos = vista.txtMinutosRecircula.getText().toString();

            String programas = Preferencias.GetPreferencia(vista, "Programas");
            programas += "\n" + infoConfig.toString();
            Preferencias.SetPreferencia(vista, "Programas", programas);

            Alerta.SuccessAlert(vista, "Se almacenó la configuración correctamente");
            vista.onBackPressed();
        }
    };

}
