package incubadora.nasoft.com.incubadoraapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class SendConfigActivity extends AppCompatActivity {


    private Spinner ddlConfiguraciones;
    private Button btnEnviarConfiguracion;
    private SendConfigController controlador;
    private FloatingActionButton fab;
    private String nombreDispositivo;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Configuraciones");
        nombreDispositivo = getIntent().getStringExtra("Dispositivo");
        controlador = new SendConfigController(this);
        SetListeners();


    }

    public String getNombreDispositivo() {
        return nombreDispositivo;
    }

    public void SetListeners() {
        ddlConfiguraciones = findViewById(R.id.ddlConfiguraciones);
        btnEnviarConfiguracion = findViewById(R.id.btnEnviarConfiguracion);
        btnEnviarConfiguracion.setOnClickListener(controlador.LoadConfiguraciones);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SendConfigActivity.this, ConfigActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        controlador.LoadPreference();
    }

    public Spinner getDdlConfiguraciones() {
        return ddlConfiguraciones;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
