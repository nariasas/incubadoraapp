package incubadora.nasoft.com.incubadoraapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class HomeActivity extends AppCompatActivity {

    private TextView txtNombre;
    private TextView txtData;
    private String nombreBT;
    private HomeController controlador;
    private ListView listaInformacion;
    private FloatingActionButton btnConfigurar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Ventana Principal");
        controlador = new HomeController(this);
        Intent abrirHome = getIntent();
        nombreBT = abrirHome.getStringExtra("Dispositivo");
        if (!nombreBT.isEmpty()) {
            ConfigurarObjetos();
        }
    }

    private void ConfigurarObjetos() {
        txtNombre = findViewById(R.id.txtNombreDispositivo);
        txtNombre.setText(nombreBT);
        txtData = findViewById(R.id.txtData);
        txtData.setText("Esperando Data...");
        txtData.setMovementMethod(new ScrollingMovementMethod());
        listaInformacion = findViewById(R.id.lista_informacion);
        btnConfigurar = findViewById(R.id.btnConfigurar);
        btnConfigurar.setOnClickListener(controlador.AbrirConfiguracion);
    }

    public TextView getTxtNombre() {
        return txtNombre;
    }

    public TextView getTxtData() {
        return txtData;
    }

    public ListView getListaInformacion()
    {
        return listaInformacion;
    }
    public FloatingActionButton getBtnConfigurar(){
        return btnConfigurar;
    }

    @Override
    protected void onResume() {
        super.onResume();
        controlador.IntentarConnectar(nombreBT);
    }

    @Override
    protected void onPause() {
        super.onPause();
        controlador.DesconectarBluetooth();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
