package incubadora.nasoft.com.incubadoraapp;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Adapter.InfoAdapter;
import Modelos.Info;
import Sistema.Alerta;
import Sistema.Bluetooth;
import Sistema.NDebug;

/**
 * Created by nicolas on 21/04/18.
 */

public class HomeController {

    private HomeActivity vista;
    private Bluetooth blue;

    public HomeController(HomeActivity _vista) {
        vista = _vista;
        blue = new Bluetooth(vista);
    }

    public void IntentarConnectar(String nombreDispositivo) {
        blue.enableBluetooth();
        final ProgressDialog cargando = Alerta.LoadingDialogOpen(vista);
        blue.connectToName(nombreDispositivo);
        blue.setOnConnect(new Bluetooth.OnConnect() {
            @Override
            public void onConnect(BluetoothDevice device, final boolean exitoso, final String msg) {

                vista.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (exitoso) {

                        } else {
                            Alerta.ErrorAlert(vista, msg);
                        }
                        VerificarConexion(exitoso);
                        Alerta.LoadingDialogClose(cargando);
                    }
                });

                if (exitoso) {
                    NDebug.Log("Se inicia proceso de lectura de data");
                    blue.setBluetoothReceivedData(new Bluetooth.BluetoothReceivedData() {
                        @Override
                        public void message(String message) {
                            ProcesarMensaje(message);
                            NDebug.Log(message);
                        }
                    });
                }


                NDebug.Log(msg);
            }
        });
    }

    private void VerificarConexion(boolean estado) {
        TextView txtNombre = vista.getTxtNombre();
        txtNombre.setTextColor(estado ? vista.getResources().getColor(R.color.green) : vista.getResources().getColor(R.color.red));
    }

    private void ProcesarMensaje(final String mensaje) {
        boolean Admin = false;
        final List<Info> informacion = new ArrayList<>();
        String token = mensaje.substring(0, 1);
        NDebug.Log(token);
        if (token.equals(">")) {
            String[] trama = mensaje.substring(1).split(",");

            String valTemp = Double.valueOf(trama[3]) < 100 ? Double.valueOf(trama[3]).toString() : (Double.valueOf(trama[3]) / 10) + "";
            String valHum = Double.valueOf(trama[4]) < 100 ? Double.valueOf(trama[4]).toString() : (Double.valueOf(trama[4]) / 10) + "";


            informacion.add(new Info("Hora", trama[0], false, R.drawable.ic_time));
            informacion.add(new Info("Temp Actual", valTemp + "°C", false, R.drawable.ic_temp));
            informacion.add(new Info("Hum Actual", valHum + "%", false, R.drawable.ic_humedad));
            informacion.add(new Info("Fase Proceso", trama[9], false, R.drawable.ic_pasos));
            informacion.add(new Info("Días Restantes", trama[10], false, R.drawable.ic_dias));
            informacion.add(new Info("Código Alarma", trama[13], false, R.drawable.ic_alarm));
            if (Admin) {
                informacion.add(new Info("TT1", trama[5], true));
                informacion.add(new Info("TT2", trama[6], true));
                informacion.add(new Info("HH1", trama[7], true));
                informacion.add(new Info("HH2", trama[8], true));
                informacion.add(new Info("# Volteos Diarios", trama[11], true));
                informacion.add(new Info("Minutos Circulación", trama[12], true));
                informacion.add(new Info("Estado Calefactor", trama[14], true));
                informacion.add(new Info("Estado Humidificador", trama[15], true));
                informacion.add(new Info("Estado Volteo", trama[16], true));
                informacion.add(new Info("Estado Circulador", trama[17], true));
                informacion.add(new Info("Temp Referencia", trama[1], true));
                informacion.add(new Info("Hum Referencia", trama[2], true));
            }
        }


        vista.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ListView listaInformacion = vista.getListaInformacion();
                InfoAdapter adapter = new InfoAdapter(vista, 0, informacion);

                listaInformacion.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                /*vista.getTxtData().append(mensaje + "\n");
                //vista.getTxtData().setMovementMethod(new ScrollingMovementMethod());
                final int scrollAmount = vista.getTxtData().getLayout().getLineTop(vista.getTxtData().getLineCount()) - vista.getTxtData().getHeight();
                // if there is no need to scroll, scrollAmount will be <=0
                if (scrollAmount > 0)
                    vista.getTxtData().scrollTo(0, scrollAmount);
                else
                    vista.getTxtData().scrollTo(0, 0);*/


            }
        });
    }

    public View.OnClickListener AbrirConfiguracion = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DesconectarBluetooth();
            Intent abrirConfiguracion = new Intent(vista, SendConfigActivity.class);
            String nombreDispositivo = vista.getTxtNombre().toString();
            abrirConfiguracion.putExtra("Dispositivo", nombreDispositivo);
            vista.startActivity(abrirConfiguracion);
        }
    };

    public void DesconectarBluetooth() {
        blue.disconnect();
    }


}
